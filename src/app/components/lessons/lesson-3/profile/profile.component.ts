import {
  ChangeDetectionStrategy,
  Component,
  Input,
  SimpleChanges,
} from '@angular/core';
import { FormArray, FormBuilder, FormControl } from '@angular/forms';

import { UserModel } from 'src/app/models/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileComponent {
  @Input() user!: UserModel;

  form = this.fb.group({
    id: [{ value: null, disabled: true }],
    name: this.fb.control(''),
    username: '',
    email: null,
    phone: new FormControl(),
    skills: this.fb.array([]),
  });

  get skills(): FormArray {
    return this.form.get('skills') as FormArray;
  }

  constructor(protected fb: FormBuilder) {}

  ngOnChanges(_changes: SimpleChanges): void {
    this.form.patchValue(this.user);
    // We dont have any skills loaded yet
    this.skills?.clear();
  }

  onSubmit = () => console.log(this.form.value);

  addSkill() {
    // TODO, design a form to add a skill to skills list
  }
}
