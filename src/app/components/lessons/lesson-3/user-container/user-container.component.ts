import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { UserModel } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-user-container',
  templateUrl: './user-container.component.html',
  styleUrls: ['./user-container.component.css'],
})
export class UserContainerComponent implements OnInit {
  users$: Observable<Array<UserModel>> = of([]);
  user!: UserModel;
  answerPage = this.route.snapshot.paramMap.get('answer');

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {
    this.users$ = this.userService.getUsers();
  }

  setUser(user: UserModel) {
    this.user = user;
  }
}
