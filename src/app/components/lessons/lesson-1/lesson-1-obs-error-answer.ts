import { of, interval } from 'rxjs';
import { catchError, finalize, take, tap } from 'rxjs/operators';
//emit error
const source = interval(1000).pipe(
  take(10),
  tap((v) => {
    //throw error for demonstration
    if (v > 5 && Math.random() >= 0.5) {
      throw new Error('Error!');
    }
  })
);
//gracefully handle error, returning observable with error message
const example = source.pipe(
  tap({ error: console.log }),
  catchError(() => of(`I caught Error`)),
  finalize(() => console.log('finalize'))
);
//output: 'I caught: This is an error'
const subscribe = example.subscribe(
  (val) => console.log(val),
  () => console.log('never'),
  () => console.log('complete')
);
