// https://rxviz.com/examples/basic-interval
const { interval, pipe } = Rx;
const { take , merge, map, mapTo, groupBy} = RxOperators;

const OB1_VAL = 'F';
const ob1 = interval(500).pipe(
  mapTo(OB1_VAL),
)

const ob2 = interval(1000).pipe(map(v => v+1));

ob1.pipe(merge(ob2), groupBy(v => v == OB1_VAL));
