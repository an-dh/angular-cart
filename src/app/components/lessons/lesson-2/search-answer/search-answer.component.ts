import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  switchMap,
  takeUntil,
} from 'rxjs/operators';
import { PostService } from 'src/app/services/post.service';
@Component({
  selector: 'app-search-answer',
  templateUrl: './search-answer.component.html',
  styleUrls: ['./search-answer.component.css'],
})
export class SearchAnswerComponent implements OnInit {
  _destroyed$ = new Subject();
  searchControl = new FormControl('');

  constructor(private postService: PostService) {}

  ngOnInit(): void {
    this.searchControl.valueChanges
      .pipe(
        debounceTime(300),
        distinctUntilChanged(),
        takeUntil(this._destroyed$),
        switchMap((searchText: string) => this.postService.getPosts(searchText))
      )
      .subscribe(console.table);
  }

  ngOnDestroy(): void {
    this._destroyed$.next();
    this._destroyed$.unsubscribe();
  }
}
