import { Component } from '@angular/core';

import { PostService } from 'src/app/services/post.service';
@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css'],
})
export class SearchComponent {
  constructor(private postService: PostService) {}

  search(e: any) {
    this.postService.getPosts(e.target.value).subscribe(console.table);
  }

  // TODO: use form control detect change
  // Do not send request if value not change in a span time (do not send in every char)
}
