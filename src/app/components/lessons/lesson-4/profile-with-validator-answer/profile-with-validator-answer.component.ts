import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

import { UserService } from 'src/app/services/user.service';
import { usernameValidator } from 'src/app/validators/username.validator';
import { ProfileAnswerComponent } from '../profile-answer/profile-answer.component';

@Component({
  selector: 'app-profile-with-validator-answer',
  templateUrl: '../../lesson-3/profile/profile.component.html',
  styleUrls: ['../../lesson-3/profile/profile.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileWithValidatorAnswerComponent extends ProfileAnswerComponent {
  form = this.formBuilder.group({
    id: [{ value: null, disabled: true }],
    name: ['', [Validators.required, Validators.maxLength(15)]],
    username: this.formBuilder.control(
      '',
      null,
      usernameValidator(this.userService)
    ),
    email: this.formBuilder.control('', [
      Validators.required,
      Validators.email,
      this.onlyClientValidator,
    ]),
    phone: this.formBuilder.control(''),
  });

  constructor(
    private userService: UserService,
    protected formBuilder: FormBuilder,
    protected matDialog: MatDialog
  ) {
    super(formBuilder, matDialog);
  }

  onlyClientValidator(control: AbstractControl): ValidationErrors | null {
    if (!control.value) {
      return null;
    }

    return control.value.endsWith('april.biz')
      ? { inactiveAccount: true }
      : null;
  }
}
