import { ValueConverter } from '@angular/compiler/src/render3/view/template';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  FormArray,
  FormControl,
  Validators,
  FormBuilder,
} from '@angular/forms';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';
import { Skill } from 'src/app/models/skill';
import { ProfileComponent } from '../../lesson-3/profile/profile.component';
import { SkillModelComponent } from './skill-model/skill-model.component';

@Component({
  selector: 'app-profile-answer',
  templateUrl: '../../lesson-3/profile/profile.component.html',
  styleUrls: ['../../lesson-3/profile/profile.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProfileAnswerComponent
  extends ProfileComponent
  implements OnInit, OnDestroy
{
  form = this.fb.group({
    id: [{ value: null, disabled: true }],
    name: ['', [Validators.required, Validators.maxLength(15)]],
    username: '',
    email: this.fb.control('', [Validators.required, Validators.email]),
    phone: new FormControl(),
    skills: this.fb.array([]),
  });

  destroyed$ = new Subject();

  constructor(protected fb: FormBuilder, protected matDialog: MatDialog) {
    super(fb);
  }

  ngOnInit(): void {
    // Set username from name
    this.form
      .get('name')
      ?.valueChanges.pipe(takeUntil(this.destroyed$))
      .subscribe((name: string) => {
        const usernameControl = this.form.get('username');
        usernameControl?.setValue(name?.replace(/ /g, '-'));
      });
  }

  ngOnDestroy(): void {
    this.destroyed$.next();
    this.destroyed$.complete();
  }

  addSkill() {
    // this.skills.push(this.fb.group({}));
    this.matDialog
      .open(SkillModelComponent, {
        width: '460px',
        data: {},
      })
      .afterClosed()
      .pipe(filter((skill) => skill))
      .subscribe((skill) => {
        this.skills.push(this.fb.group(skill));
        this.form.updateValueAndValidity();
      });
  }
}
