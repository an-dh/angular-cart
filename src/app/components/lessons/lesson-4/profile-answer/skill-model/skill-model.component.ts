import { Component, Inject, Input, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Skill } from 'src/app/models/skill';

@Component({
  selector: 'app-skill-model',
  templateUrl: './skill-model.component.html',
  styleUrls: ['./skill-model.component.css'],
})
export class SkillModelComponent implements OnInit {
  form = this.fb.group({ name: ['', Validators.required], exp: '' });

  constructor(
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: Skill
  ) {}

  ngOnInit(): void {
    this.form.patchValue(this.data);
  }
}
