import { Component, OnDestroy, OnInit } from '@angular/core';
import { NG_ASYNC_VALIDATORS } from '@angular/forms';
import {
  fromEvent,
  interval,
  observable,
  Observable,
  Subject,
  Subscription,
} from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-unsubscribe',
  templateUrl: './unsubscribe.component.html',
  styleUrls: ['./unsubscribe.component.css'],
})
export class UnsubscribeComponent implements OnInit, OnDestroy {
  sub?: Subscription;
  _destroyed$ = new Subject();

  ngOnInit() {
    // Observables
    let interval$ = interval(5000);
    let interval2$ = fromEvent(document, 'click');

    // subscriptions managed using on Subscription
    this.sub = interval$.subscribe(console.log);
    this.sub = interval2$.subscribe(console.log);

    // subscription managed until an event
    interval$
      .pipe(takeUntil(this._destroyed$))
      .subscribe((data) => console.log(data + ' - takeUtil'));
  }
  ngOnDestroy(): void {
    this.sub?.unsubscribe();

    // notice (send a event)
    this._destroyed$.next();
    this._destroyed$.complete();
  }
}
