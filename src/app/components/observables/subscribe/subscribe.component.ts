import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material/snack-bar';
import { interval, observable, Observable, of } from 'rxjs';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css'],
})
export class SubscribeComponent implements OnInit {
  interval$?: Observable<number>;

  constructor(private snackbar: MatSnackBar) {}

  ngOnInit() {
    this.interval$ = interval(5000);
    this.interval$.subscribe((data) => {
      this.snackbar.open(`remember to unsubscribe  me (${data})`, undefined, {
        horizontalPosition: 'right',
      });
    });
  }
}
