import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ObservableComponent } from './components/observables/observable-container/observable.component';
import { SearchComponent } from './components/lessons/lesson-2/search/search.component';
import { AppComponent } from './app.component';
import { SearchAnswerComponent } from './components/lessons/lesson-2/search-answer/search-answer.component';
import { UserContainerComponent } from './components/lessons/lesson-3/user-container/user-container.component';

const routes: Routes = [
  { path: 'observable', component: ObservableComponent },
  { path: 'search', component: SearchComponent },
  { path: 'search/:answer', component: SearchAnswerComponent },
  { path: 'users', component: UserContainerComponent },
  { path: 'users/:answer', component: UserContainerComponent },
  { path: '', component: AppComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
