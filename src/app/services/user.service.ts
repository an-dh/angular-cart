import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModel } from '../models/user.model';
import { PostModel } from '../models/post.model';

@Injectable({ providedIn: 'root' })
export class UserService {
  constructor(private httpClient: HttpClient) {}

  getProfile(): Observable<UserModel> {
    return this.httpClient.get<UserModel>(
      'https://jsonplaceholder.typicode.com/users/1'
    );
  }
  getUsers(): Observable<Array<UserModel>> {
    return this.httpClient.get<Array<UserModel>>(
      'https://jsonplaceholder.typicode.com/users'
    );
  }
  getPosts(): Observable<Array<PostModel>> {
    return this.httpClient.get<Array<PostModel>>(
      'https://jsonplaceholder.typicode.com/posts'
    );
  }
}
