import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {
  MatSnackBarModule,
  MAT_SNACK_BAR_DEFAULT_OPTIONS,
} from '@angular/material/snack-bar';
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDialogModule } from '@angular/material/dialog';
import { MatInputModule } from '@angular/material/input';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { SubscribeComponent } from './components/observables/subscribe/subscribe.component';
import { ObservableComponent } from './components/observables/observable-container/observable.component';
import { UnsubscribeComponent } from './components/observables/unsubscrible/unsubscribe.component';
import { SearchComponent } from './components/lessons/lesson-2/search/search.component';
import { SearchAnswerComponent } from './components/lessons/lesson-2/search-answer/search-answer.component';
import { ProfileComponent } from './components/lessons/lesson-3/profile/profile.component';
import { UserContainerComponent } from './components/lessons/lesson-3/user-container/user-container.component';
import { MenuComponent } from './components/menu/menu.component';
import { ProfileAnswerComponent } from './components/lessons/lesson-4/profile-answer/profile-answer.component';
import { ProfileWithValidatorAnswerComponent } from './components/lessons/lesson-4/profile-with-validator-answer/profile-with-validator-answer.component';
import { SkillModelComponent } from './components/lessons/lesson-4/profile-answer/skill-model/skill-model.component';

@NgModule({
  declarations: [
    AppComponent,
    ObservableComponent,
    ProfileComponent,
    ProfileAnswerComponent,
    ProfileWithValidatorAnswerComponent,
    SearchComponent,
    SearchAnswerComponent,
    SubscribeComponent,
    UnsubscribeComponent,
    UserContainerComponent,
    MenuComponent,
    SkillModelComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    MatInputModule,
    MatFormFieldModule,
    MatProgressSpinnerModule,
    MatSnackBarModule,
    MatListModule,
    MatDialogModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [
    { provide: MAT_SNACK_BAR_DEFAULT_OPTIONS, useValue: { duration: 2500 } },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
