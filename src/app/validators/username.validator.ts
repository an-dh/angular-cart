import {
  AbstractControl,
  AsyncValidatorFn,
  ValidationErrors,
} from '@angular/forms';
import { Observable, of } from 'rxjs';
import { delay, mapTo } from 'rxjs/operators';
import { UserService } from '../services/user.service';
export function usernameValidator(userService: UserService): AsyncValidatorFn {
  return (control: AbstractControl): Observable<ValidationErrors | null> => {
    if (!control.value || control.value.length < 5) {
      return of(null);
    }

    return userService
      .getProfile()
      .pipe(mapTo(Math.random() >= 0.5 ? { existing: true } : null));

    // return of({ existing: true }).pipe(delay(200));
  };
}
