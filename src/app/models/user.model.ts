import { PostModel } from './post.model';

export interface UserModel {
  id: string;
  name: string;
  username: string;
  email: string;
  phone: string;
}
